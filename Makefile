nix:
	nix-shell

krita:
	nix-shell -p krita

inkscape:
	nix-shell -p inkscape

show:
	falkon index.html

