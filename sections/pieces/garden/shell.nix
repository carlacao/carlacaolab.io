{ pkgs ? import <nixpkgs> { } }:

let
  xls2csv = pkgs.writeShellScriptBin "ss" ''

Help()
{
   # Display Help
   echo "Add description of the script functions here."
   echo
   echo "Syntax: scriptTemplate [-g|h|v|V]"
   echo "options:"
   echo "g     Print the GPL license notification."
   echo "h     Print this Help."
   echo "v     Verbose mode."
   echo "V     Print software version and exit."
   echo
}

while getopts ":h" option; do
   case $option in
      h) # display Help
         Help
         exit;;
   esac
done

ssconvert $1 "${$2}.csv"


'';

in
pkgs.mkShell {
  buildInputs = with pkgs; [
    gnumeric
  ];
}
